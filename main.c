#include <mask.h>
#include<stdio.h>

static int mask_owner, mask_group, mask_other;

int main() {
	char ro,wo,xo,rg,wg,xg,rot,wot,xot,useless_buffer;
	printf("Permiso de lectura para owmer? [s/n]");
	scanf("%c",&ro);
	scanf("%c",&useless_buffer);//vaciando el enter al final :v
	if (ro=='s'){
		setPermiso(&mask_owner,'r',SET);
	}
	else if (ro=='n'){
		setPermiso(&mask_owner,'r',UNSET);
	}
	printf("Permiso de escritura para owmer? [s/n]");
	scanf("%c",&wo);
	scanf("%c",&useless_buffer);//vaciando el enter al final :v
	if (wo=='s'){
		setPermiso(&mask_owner,'w',SET);
	}
	else if (wo=='n'){
		 setPermiso(&mask_owner,'w',UNSET);
	}
	printf("Permiso de ejecucion para owner? [s/n]");
	scanf("%c",&xo);
	scanf("%c",&useless_buffer);//vaciando el enter al final :v
	if (xo=='s'){
		setPermiso(&mask_owner,'x',SET);
	}
	else if (xo=='n'){
		setPermiso(&mask_owner,'x',UNSET);
	}
	printf("Permiso de lectura para group? [s/n]");
	scanf("%c",&rg);
	scanf("%c",&useless_buffer);//vaciando el enter al final :v
	if (rg=='s'){
		setPermiso(&mask_group,'r',SET);
	}
	else if (rg=='n'){
		setPermiso(&mask_group,'r',UNSET);
	}
	printf("Permiso de escritura para group? [s/n]");
	scanf("%c",&wg);
	scanf("%c",&useless_buffer);//vaciando el enter al final :v
	if (wg=='s'){
		setPermiso(&mask_group,'w',SET);
	}
	else if (wg=='n'){
		 setPermiso(&mask_group,'w',UNSET);
	}
	printf("Permiso de ejecucion para group? [s/n]");
	scanf("%c",&xg);
	scanf("%c",&useless_buffer);//vaciando el enter al final :v
	if (xg=='s'){
		setPermiso(&mask_group,'x',SET);
	}
	else if (xg=='n'){
		setPermiso(&mask_group,'x',UNSET);
	}
	printf("Permiso de lectura para others? [s/n]");
	scanf("%c",&rot);
	scanf("%c",&useless_buffer);//vaciando el enter al final :v
	if (rot=='s'){
		setPermiso(&mask_other,'r',SET);
	}
	else if (rot=='n'){
		setPermiso(&mask_other,'r',UNSET);
	}
	printf("Permiso de escritura para others? [s/n]");
	scanf("%c",&wot);
	scanf("%c",&useless_buffer);//vaciando el enter al final :v
	if (wot=='s'){
		setPermiso(&mask_other,'w',SET);
	}
	else if (wot=='n'){
		 setPermiso(&mask_other,'w',UNSET);
	}
	printf("Permiso de ejecucion para others? [s/n]");
	scanf("%c",&xot);
	scanf("%c",&useless_buffer);//vaciando el enter al final :v
	if (xot=='s'){
		setPermiso(&mask_other,'x',SET);
	}
	else if (xot=='n'){
		setPermiso(&mask_other,'x',UNSET);
	}
	printf("Los permisos dados son: %i%i%i\n",mask_owner,mask_group,mask_other);

}
