CC = gcc
CFLAGS= -c -I.
DEPS = mask.h

all: salida

salida: main.o mask.o
	$(CC) -o salida main.o mask.o -I.

%.o: %.c $(DEPS)
	$(CC) $(CFLAGS) -o $@ $<

clear:
	rm *o salida
